package com.compdigitec.libvlcandroidsample;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;
import org.videolan.libvlc.util.AndroidUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {
    public final static String TAG = "MainActivity";

    DirectoryAdapter mAdapter;
    LibVLC mLibVLC = null;
    MediaPlayer mMediaPlayer = null;

    private final  int MY_PERMISSIONS_REQUEST = 100;

    boolean mPlayingVideo = false; // Don't destroy libVLC if the video activity is playing.

    View.OnClickListener mSimpleListener = new View.OnClickListener() {
        @Override
        public void onClick(View arg0) {
            Intent intent = new Intent(MainActivity.this, VideoActivity.class);
            //String url = "rtsp://mpv.cdn3.bigCDN.com:554/bigCDN/mp4:bigbuckbunnyiphone_400.mp4";
            String url = "rtsp://fastweb.almoonds.com:554";
            intent.putExtra(VideoActivity.LOCATION, url );
            mPlayingVideo = true;
            startActivity(intent);
        }
    };

    /**
     * Demonstrates how to play a certain media at a given path.
     * TODO: demonstrate other LibVLC features like media lists, etc.
     */
    private void playMediaAtPath(String path) {
        // To play with LibVLC, we need a media player object.
        // Let's get one, if needed.
        if(mMediaPlayer == null)
            mMediaPlayer = new MediaPlayer(mLibVLC);

        // Sanity check - make sure that the file exists.
        if(!new File(path).exists()) {
            Toast.makeText(
                    MainActivity.this,
                    path + " does not exist!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        // Create a new Media object for the file.
        // Each media - a song, video, or stream is represented by a Media object for LibVLC.
        Media m = new Media(mLibVLC, path);

        // Tell the media player to play the new Media.
        mMediaPlayer.setMedia(m);

        // Finally, play it!
        mMediaPlayer.play();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Initialize the LibVLC multimedia framework.
        // This is required before doing anything with LibVLC.
        try {
            //mLibVLC = new LibVLC(getApplicationContext());
            mLibVLC = new LibVLC();
        } catch(IllegalStateException e) {
            Toast.makeText(MainActivity.this,
                    "Error initializing the libVLC multimedia framework!",
                    Toast.LENGTH_LONG).show();
            finish();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermissionsForAboveMarshmallow();
    }

    private void setupUIElements() {
        mAdapter = new DirectoryAdapter();
        Button load_a_mp3 = (Button) findViewById(R.id.load_a_mp3);
        load_a_mp3.setOnClickListener(mSimpleListener);
        final ListView mediaView = (ListView) findViewById(R.id.mediaView);
        mediaView.setAdapter(mAdapter);
        mediaView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                    int position, long arg3) {
                if (mAdapter.isAudioMode()) {
                    playMediaAtPath((String) mAdapter.getItem(position));
                } else {
                    Intent intent = new Intent(MainActivity.this, VideoActivity.class);
                    intent.putExtra(VideoActivity.LOCATION, (String) mAdapter.getItem(position));
                    mPlayingVideo = true;
                    startActivity(intent);
                }
            }
        });
        RadioButton radioAudio = (RadioButton)findViewById(R.id.radioAudio);
        radioAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.setAudioMode(true);
                mAdapter.refresh();
            }
        });
        RadioButton radioVideo = (RadioButton)findViewById(R.id.radioVideo);
        radioVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.setAudioMode(false);
                mAdapter.refresh();
            }
        });
    }

    private void checkPermissionsForAboveMarshmallow() {

        List<String> listReqPermission = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                listReqPermission.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            if(listReqPermission.size() > 0) {
                ActivityCompat.requestPermissions(this,listReqPermission.toArray(new String[listReqPermission.size()]),
                        MY_PERMISSIONS_REQUEST);
            } else {
                setupUIElements();
            }
        } else {
            setupUIElements();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST: {

                int i = 0;
                for( String permission : permissions ) {
                    Log.d("permission", ""+ permission + " = " + grantResults[i]);
                    ++i;
                }

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // 권한이 부여되었을 경우
                    setupUIElements();
                } else {
                    // 권한이 부여되지 않았을 경우
                    Toast.makeText(getApplicationContext(), permissions[0] + " permission denied!", Toast.LENGTH_LONG).show();

                    if (permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE) ) {
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPlayingVideo = false;
    };

    @Override
    public void onStop() {
        super.onStop();
        if(mPlayingVideo) {
            if( null != mMediaPlayer ) {
                Log.e(TAG, "start elapsedTime mediaPlayer stop");
                long prev = System.currentTimeMillis();
                mMediaPlayer.stop();
                long elapsedTime = System.currentTimeMillis() - prev;
                Log.e(TAG, "stop elapsedTime:" + elapsedTime/1000.);
            }
        }

        if( null != mLibVLC ) {
            mLibVLC.release();
            mLibVLC = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
        case R.id.action_settings:
            Log.d(TAG, "Setting item selected.");
            return true;
        case R.id.action_refresh:
            mAdapter.refresh();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
}
